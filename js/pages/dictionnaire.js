// JavaScript Document for Home Page
$(document).ready(function(e) {	
	$('.windows-main').append(
		'<div class="container">'+
			'<div class="row tab first-tab center">'+
				'<div class="col-md-12">'+
					'<h1>Dictionnaire BJC</h1>'+
					'<p>« <em>Alors je tombai à ses pieds pour l\'adorer, mais il me dit : Garde-toi de le faire ! Je suis ton compagnon de service, et celui de tes frères qui ont le témoignage de Yéhoshoua. Adore Elohîm ! Car le témoignage de Yéhoshoua est l\'Esprit de la prophétie.</em> » Apocalypse 19:10</p>'+
				'</div>'+
			'</div>'+
			'<div class="row tab other-tab">'+
				'<div class="col-md-12 section" data-textid="fra_bjc">'+
					'<table id="dic_table" class="table table-striped table-bordered">'+
						'<thead>'+
							'<tr>'+
								'<th>Terme</th>'+
								'<th>Etymologie</th>'+
								'<th>Définition</th>'+
							'</tr>'+
						'</thead>'+
						'<tfoot>'+
							'<tr>'+
								'<th>Terme</th>'+
								'<th>Etymologie</th>'+
								'<th>Définition</th>'+
							'</tr>'+
						'</tfoot>'+
					'</table>'+
				'</div>'+
			'</div>'+
		'</div>'
	);
	$(document).ready( function () {
		var table = $('#dic_table').DataTable( {
			"ajax": "content/dictionaries/dictionnaire.json",
			"columns": [
				{ "data": "word" },
				{ "data": "etymology" },
				{ "data": "definition" }
			],
			 "language": {
				"lengthMenu": "Afficher _MENU_ enregistrements par page",
				"zeroRecords": "Aucun résultat trouvé",
				"info": "Affichage de la page _PAGE_ sur _PAGES_",
				"infoEmpty": "Aucun enregistrement disponible",
				"infoFiltered": "(filtered from _MAX_ total records)",
				"paginate": {
					"next": "Suivant",
					"previous": "Précédent"
					},
				"search": "Recherche"
			},
			"pageLength": 25,
			"autoWidth": false,
			"responsive": true,
		 } );
 
		table.on( 'draw', function () {
			var body = $( table.table().body() );
	 
			body.unhighlight();
			if ( table.rows( { filter: 'applied' } ).data().length ) {
				body.highlight( table.search() );
			}
		} );
	} );

});	
	
