sofia.resources['en'] = {
	"translation": {
		"name": "English",
		"menu": {
			"search": {
				"placeholder": "Search"
			},
			"config": {
				"font": "Style",
				"settings": "Settings",
				"tools": "Tools"
			},
			"reset": "restore",
			"themes": {
				"default": "Normal",
				"sepia": "Sepia",
				"dark": "Low Light"
			},
			"bjc": {
				"home": "Home",
				"bible": "Bible",
				"download": "Download",
				"order": "Order",
				"donate": "Donate",
				"contact": "Contact",
				"comparison": "Comparison"
				}
		},
		"plugins": {
			"visualfilters": {
				"button": "Visual Filters",
				"title": "Visual Filters",
				"newfilter": "New Filter",
				"strongsnumber": "Strong's #",
				"morphology": "Morphology",
				"style": "Style"
			},
			"eng2p": {
				"button": "English 2nd Person Plural",
				"title": "English Second Person Plural",
				"description": "Modern English does not distinguish between <em>you singular</em> and <em>you plural</em>, so this plugin uses regional English equivalents to represent the Hebrew and Greek forms."
			},
			"lemmapopup": {
				"findalloccurrences": "Find all occurrences (approximately __count__)"
			}
		},
		"windows": {
			"bible": {
				"label": "Bible",
				"recentlyused": "Recently Used",
				"filter": "Filter...",
				"ot": "Old Testament",
				"nt": "New Testament",
				"dc": "Deuterocanonical Books",
				"more": "More",
				"less": "Less",
				"languages": "Languages",
				"countries": "Countries"
			},
			"commentary": {
				"label": "Commentary"
			},
			"map": {
				"label": "Map",
				"placeholder": "Search..."
			},
			"search": {
				"label": "Search",
				"placeholder": "Search",
				"button": "Search",
				"results": "Results",
				"verses": "verses",
				"options": "Search Options"							
			},
			"parallel": {
				"label": "Parallels",
				"loading": "Loading...",
				"showall": "Show All",
				"hideall": "Hide All"
			},
			"media": {
				"label": "Media"
			},
			"notes": {
				"label": "Notes"
			},
			"dictionary": {
				"label": "Dictionary"
			},
			"gralphabet": {
				"label": "Greek Alphabet"
			},
			"hebalphabet": {
				"label": "Hebrew Alphabet"
			},
			"audio": {
				"options": "Audio Options",
				"synctext": "Sync Text (beta)",
				"autoplay": "Autoplay Next",
				"drama": "Dramatized",
				"nondrama": "Spoken"
			},
			"tooltips": {
				"textnav": "Reference",
				"textlist": "Version",
				"info": "Version Information",
				"audio": "Listen"
			},
			"comparison": {
				"label": "Comparison"				
			}
		},
		"names": {
			"en": "English",
			"de": "Englisch",
			"zh-TW": "英語",
			"zh-CN": "英语",
			"es": "Inglés",
			"fr": "anglais"
		}
	}
}
