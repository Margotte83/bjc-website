<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
<title>Ezéchiel 20 (BJC)</title>
<link href="../../../build/mobile.css" rel="stylesheet" />
<script src="../../../build/mobile.js"></script>
</head>
<body dir="ltr" class="section-document">
<div class="header"><div class="nav">
<a class="name" href="EK.html">Bible de Jésus-Christ</a><a class="location" href="EK.html">Ezéchiel 20</a><a class="prev" href="EK19.html">&lt;</a>
<a class="home" href="index.html">=</a>
<a class="next" href="EK21.html">&gt;</a>
</div></div>
<div class="section chapter EK EK20 fra_bjc fra" dir="ltr" lang="fr" data-id="EK20" data-nextid="EK21" data-previd="EK19">
<div class="c">20</div>
<div class="p">
		<div class="s">Compassions de YHWH face aux infidélités d'Israël</div>
		<span class="v-num v-1">1&nbsp;</span><span id="EK20_1" class="v EK20_1" data-id="EK20_1">Or il arriva la septième année, au dixième jour du cinquième mois, que quelques-uns des anciens d'Israël vinrent pour consulter YHWH, et s'assirent devant moi.</span>

		<span class="v-num v-2">2&nbsp;</span><span id="EK20_2" class="v EK20_2" data-id="EK20_2">La parole de YHWH vint à moi en disant :</span>

		<span class="v-num v-3">3&nbsp;</span><span id="EK20_3" class="v EK20_3" data-id="EK20_3">Fils de l'homme, parle aux anciens d'Israël, et dis-leur : Ainsi parle Adonaï YHWH : Est-ce pour me consulter que vous venez ? Je suis vivant, dit Adonaï YHWH, je ne me laisserai pas consulter par vous !</span>

		<span class="v-num v-4">4&nbsp;</span><span id="EK20_4" class="v EK20_4" data-id="EK20_4">Ne les jugeras-tu pas, ne les jugeras-tu pas, fils de l'homme ? Fais-leur connaître les abominations de leurs pères.</span>

		<span class="v-num v-5">5&nbsp;</span><span id="EK20_5" class="v EK20_5" data-id="EK20_5">Et dis-leur : Ainsi parle Adonaï YHWH : Le jour où j'ai choisi Israël, j'ai levé ma main vers la postérité de la maison de Yaacov et je me suis fait connaître à eux dans le pays d'Égypte. J'ai levé ma main vers eux en disant : Je suis YHWH, votre Elohîm.</span>

		<span class="v-num v-6">6&nbsp;</span><span id="EK20_6" class="v EK20_6" data-id="EK20_6">En ce jour, j'ai levé ma main vers eux pour les faire sortir du pays d'Égypte vers un pays que j'avais cherché pour eux, un pays où coulent le lait et le miel, et qui est la beauté de tous les pays<span class="note" id="note-1"><a class="key" href="#footnote-1">1</a></span>.</span>

		<span class="v-num v-7">7&nbsp;</span><span id="EK20_7" class="v EK20_7" data-id="EK20_7">Alors je leur dis : Que chacun de vous rejette les abominations qui attirent ses yeux ! Ne vous souillez pas avec les idoles d'Égypte ! Je suis YHWH, votre Elohîm<span class="note" id="note-2"><a class="key" href="#footnote-2">2</a></span>.</span>

		<span class="v-num v-8">8&nbsp;</span><span id="EK20_8" class="v EK20_8" data-id="EK20_8">Mais ils se sont révoltés contre moi et n'ont pas voulu m'écouter. Aucun d'eux n'a rejeté les abominables qu'il avait sous les yeux et ils n'ont pas abandonné les idoles de l'Égypte. J'ai parlé de répandre ma fureur sur eux, d'accomplir ma colère contre eux au milieu du pays d'Égypte.</span>

		<span class="v-num v-9">9&nbsp;</span><span id="EK20_9" class="v EK20_9" data-id="EK20_9">Mais j'ai agi à cause de mon Nom, afin qu'il ne soit pas profané aux yeux des nations parmi lesquelles ils se trouvaient. En effet, c'était sous leurs yeux que je m'étais fait connaître à eux pour les faire sortir du pays d'Égypte.</span>

		<span class="v-num v-10">10&nbsp;</span><span id="EK20_10" class="v EK20_10" data-id="EK20_10">Je les ai donc fait sortir d'Égypte et je les ai conduits dans le désert.</span>

		<span class="v-num v-11">11&nbsp;</span><span id="EK20_11" class="v EK20_11" data-id="EK20_11">Je leur ai donné mes lois et leur ai fait connaître mes ordonnances, celles que l'être humain doit mettre en pratique afin de vivre par elles<span class="note" id="note-3"><a class="key" href="#footnote-3">3</a></span>.</span>

		<span class="v-num v-12">12&nbsp;</span><span id="EK20_12" class="v EK20_12" data-id="EK20_12">Je leur ai donné aussi mes shabbats pour être un signe entre moi et eux afin qu'ils sachent que je suis YHWH qui les sanctifie<span class="note" id="note-4"><a class="key" href="#footnote-4">4</a></span>.</span>

		<span class="v-num v-13">13&nbsp;</span><span id="EK20_13" class="v EK20_13" data-id="EK20_13">Mais ceux de la maison d'Israël se sont rebellés contre moi dans le désert. Ils n'ont pas suivi mes lois, et ils ont rejeté mes ordonnances, celles que l'être humain doit mettre en pratique afin de vivre par elles, et ils ont profané à l'excès mes shabbats. C'est pourquoi j'ai parlé de répandre sur eux ma colère dans le désert pour les consumer<span class="note" id="note-5"><a class="key" href="#footnote-5">5</a></span>.</span>

		<span class="v-num v-14">14&nbsp;</span><span id="EK20_14" class="v EK20_14" data-id="EK20_14">J'ai agi à cause de mon Nom, afin de ne pas le profaner aux yeux des nations sous les yeux desquelles je les avais fait sortir<span class="note" id="note-6"><a class="key" href="#footnote-6">6</a></span>.</span>

		<span class="v-num v-15">15&nbsp;</span><span id="EK20_15" class="v EK20_15" data-id="EK20_15">Dans le désert, c'est aussi moi qui leur ai juré, à main levée, de ne pas les conduire dans le pays que je leur avais donné, pays où coulent le lait et le miel, et qui est la beauté de tous les pays.</span>

		<span class="v-num v-16">16&nbsp;</span><span id="EK20_16" class="v EK20_16" data-id="EK20_16">Parce qu'ils rejetaient mes ordonnances, qu'ils ne suivaient pas mes lois, qu'ils profanaient mes shabbats, car leur cœur marchait après leurs idoles.</span>

		<span class="v-num v-17">17&nbsp;</span><span id="EK20_17" class="v EK20_17" data-id="EK20_17">Toutefois mon œil les a épargnés pour ne pas les détruire et je n'ai pas fait d'eux une destruction complète dans le désert.</span>

		<span class="v-num v-18">18&nbsp;</span><span id="EK20_18" class="v EK20_18" data-id="EK20_18">Cependant, j'ai dit à leurs enfants dans le désert : Ne marchez pas dans les statuts de vos pères, et ne gardez pas leurs ordonnances, et ne vous souillez pas par leurs idoles.</span>

		<span class="v-num v-19">19&nbsp;</span><span id="EK20_19" class="v EK20_19" data-id="EK20_19">Je suis YHWH, votre Elohîm. Marchez selon mes statuts, gardez mes ordonnances et accomplissez-les !</span>

		<span class="v-num v-20">20&nbsp;</span><span id="EK20_20" class="v EK20_20" data-id="EK20_20">Sanctifiez mes shabbats et qu'ils soient un signe entre moi et vous par lequel on saura que je suis YHWH votre Elohîm !</span>

		<span class="v-num v-21">21&nbsp;</span><span id="EK20_21" class="v EK20_21" data-id="EK20_21">Mais leurs enfants se sont rebellés contre moi. Ils n'ont pas marché selon mes statuts et n'ont pas gardé mes ordonnances pour les mettre en pratique, celles que l'être humain doit accomplir afin de vivre par elles et ils ont profané mes shabbats. C'est pourquoi j'ai parlé de répandre sur eux ma colère, d'accomplir ma colère contre eux dans le désert.</span>

		<span class="v-num v-22">22&nbsp;</span><span id="EK20_22" class="v EK20_22" data-id="EK20_22">Toutefois, j'ai retiré ma main et j'ai agi à cause de mon Nom, afin de ne pas le profaner aux yeux des nations sous les yeux desquelles je les avais fait sortir.</span>

		<span class="v-num v-23">23&nbsp;</span><span id="EK20_23" class="v EK20_23" data-id="EK20_23">Dans le désert, c'est aussi moi qui leur ai juré, à main levée, de les éparpiller parmi les nations, de les disperser dans tous les pays<span class="note" id="note-7"><a class="key" href="#footnote-7">7</a></span>,</span>

		<span class="v-num v-24">24&nbsp;</span><span id="EK20_24" class="v EK20_24" data-id="EK20_24">parce qu'ils n'ont pas accompli mes ordonnances, qu'ils ont rejeté mes statuts, profané mes shabbats et que leurs yeux se sont attachés aux idoles de leurs pères.</span>

		<span class="v-num v-25">25&nbsp;</span><span id="EK20_25" class="v EK20_25" data-id="EK20_25">À cause de cela, je leur ai même donné des statuts qui n'étaient pas bons et des ordonnances par lesquelles ils ne pouvaient pas vivre.</span>

		<span class="v-num v-26">26&nbsp;</span><span id="EK20_26" class="v EK20_26" data-id="EK20_26">Je les ai souillés par leurs dons, quand ils faisaient passer par le feu tout ce qui sort le premier du sein maternel, pour les frapper de stupeur afin qu'ils sachent que je suis YHWH.</span>

		<span class="v-num v-27">27&nbsp;</span><span id="EK20_27" class="v EK20_27" data-id="EK20_27">C'est pourquoi, toi fils de l'homme, parle à la maison d'Israël et dis-leur : Ainsi parle Adonaï YHWH : Vos pères m'ont encore blasphémé en se montrant infidèles envers moi.</span>

		<span class="v-num v-28">28&nbsp;</span><span id="EK20_28" class="v EK20_28" data-id="EK20_28">Je les ai fait entrer dans le pays que j'avais juré, à main levée, de leur donner, et ils ont regardé chaque colline élevée et chaque arbre touffu. C'est là qu'ils ont fait leurs sacrifices, qu'ils ont donné leurs offrandes pour m'irriter, qu'ils ont mis leurs parfums apaisants et qu'ils ont répandu leurs libations.</span>

		<span class="v-num v-29">29&nbsp;</span><span id="EK20_29" class="v EK20_29" data-id="EK20_29">Je leur ai dit : Que veulent dire ces hauts lieux où vous allez ? Et le nom de hauts lieux leur a été donné jusqu'à ce jour.</span>

		<span class="v-num v-30">30&nbsp;</span><span id="EK20_30" class="v EK20_30" data-id="EK20_30">C'est pourquoi dis à la maison d'Israël : Ainsi parle Adonaï YHWH : Ne vous souillez-vous pas en suivant les voies de vos pères et ne vous prostituez-vous pas à leurs idoles abominables ?</span>

		<span class="v-num v-31">31&nbsp;</span><span id="EK20_31" class="v EK20_31" data-id="EK20_31">En offrant vos dons, en faisant passer vos fils par le feu, en vous souillant par toutes vos idoles jusqu'à ce jour, est-ce ainsi que vous me consultez, ô maison d'Israël ? Je suis vivant, dit Adonaï YHWH, vous ne me consultez pas.</span>

		<span class="v-num v-32">32&nbsp;</span><span id="EK20_32" class="v EK20_32" data-id="EK20_32">Ce qui vous monte à l'esprit n'arrivera nullement, quand vous dites : Nous serons comme les nations, comme les familles des pays, en servant le bois et la pierre.</span>

		<div class="s">Restauration future d'Israël</div>
		<span class="v-num v-33">33&nbsp;</span><span id="EK20_33" class="v EK20_33" data-id="EK20_33">Je suis vivant ! dit Adonaï YHWH. Je règnerai sur vous d'une main forte, d'un bras étendu, en déversant ma colère.</span>

		<span class="v-num v-34">34&nbsp;</span><span id="EK20_34" class="v EK20_34" data-id="EK20_34">Je vous sortirai du milieu des peuples et je vous rassemblerai hors des pays où vous êtes dispersés, d'une main forte, d'un bras étendu, en déversant ma colère.</span>

		<span class="v-num v-35">35&nbsp;</span><span id="EK20_35" class="v EK20_35" data-id="EK20_35">Je vous ferai venir dans le désert des peuples et là, je vous jugerai face à face,</span>

		<span class="v-num v-36">36&nbsp;</span><span id="EK20_36" class="v EK20_36" data-id="EK20_36">comme j'ai jugé vos pères dans le désert du pays d'Égypte, ainsi vous jugerai-je, dit Adonaï YHWH.</span>

		<span class="v-num v-37">37&nbsp;</span><span id="EK20_37" class="v EK20_37" data-id="EK20_37">Je vous ferai passer sous la verge et je vous introduirai dans le lien de l'alliance<span class="note" id="note-8"><a class="key" href="#footnote-8">8</a></span>.</span>

		<span class="v-num v-38">38&nbsp;</span><span id="EK20_38" class="v EK20_38" data-id="EK20_38">Je séparerai de vous les rebelles, ceux qui se révoltent contre moi. Je les ferai sortir de la terre où ils séjournent, mais ils ne viendront pas sur le sol d'Israël. Vous saurez que je suis YHWH.</span>

		<span class="v-num v-39">39&nbsp;</span><span id="EK20_39" class="v EK20_39" data-id="EK20_39">Vous donc, ô maison d'Israël, ainsi parle Adonaï YHWH : Que chaque homme aille donc servir ses idoles ! Mais après cela, ne m'écouterez-vous pas ? Ainsi vous ne profanerez plus mon saint Nom par vos dons et par vos idoles.</span>

		<span class="v-num v-40">40&nbsp;</span><span id="EK20_40" class="v EK20_40" data-id="EK20_40">Mais ce sera sur ma sainte montagne, sur la haute montagne d'Israël, dit Adonaï YHWH, que toute la maison d'Israël me servira, dans le pays<span class="note" id="note-9"><a class="key" href="#footnote-9">9</a></span>. Là, je prendrai plaisir en eux, et là je demanderai vos contributions et le meilleur de vos dons, et tout ce que vous me consacrerez.</span>

		<span class="v-num v-41">41&nbsp;</span><span id="EK20_41" class="v EK20_41" data-id="EK20_41">Je vous accueillerai favorablement, comme un sacrifice dont le parfum est apaisant, quand je vous aurai fait sortir du milieu des peuples et que je vous aurai rassemblés des pays où vous êtes dispersés. Je serai sanctifié par vous, aux yeux des nations.</span>

		<span class="v-num v-42">42&nbsp;</span><span id="EK20_42" class="v EK20_42" data-id="EK20_42">Vous saurez que je suis YHWH, quand je vous aurai fait revenir dans le pays d'Israël, dans le pays au sujet duquel j'ai levé ma main pour le donner à vos pères.</span>

		<span class="v-num v-43">43&nbsp;</span><span id="EK20_43" class="v EK20_43" data-id="EK20_43">Et là, vous vous souviendrez de vos voies, et de toutes vos actions par lesquelles vous vous êtes rendus impurs. Vous vous prendrez vous-mêmes en dégoût à cause de tout le mal que vous avez commis.</span>

		<span class="v-num v-44">44&nbsp;</span><span id="EK20_44" class="v EK20_44" data-id="EK20_44">Vous saurez que je suis YHWH, quand j'agirai avec vous à cause de mon Nom, et non pas selon vos méchantes voies et vos actions corrompues, ô maison d'Israël ! dit Adonaï YHWH.</span>

	</div>
</div>
<div class="footnotes">
<span class="footnote" id="footnote-1"><span class="key">1</span><a class="backref" href="#note-1">20:6</a><span class="text">Ex. 3:8, 6:7.</span></span><span class="footnote" id="footnote-2"><span class="key">2</span><a class="backref" href="#note-2">20:7</a><span class="text">Jos. 24:14-23.</span></span><span class="footnote" id="footnote-3"><span class="key">3</span><a class="backref" href="#note-3">20:11</a><span class="text">Lé. 18:5 ; Ro. 10:5 ; Ga. 3:12.</span></span><span class="footnote" id="footnote-4"><span class="key">4</span><a class="backref" href="#note-4">20:12</a><span class="text">Ex. 20:8, 31:13.</span></span><span class="footnote" id="footnote-5"><span class="key">5</span><a class="backref" href="#note-5">20:13</a><span class="text">Ex. 16:28.</span></span><span class="footnote" id="footnote-6"><span class="key">6</span><a class="backref" href="#note-6">20:14</a><span class="text">Ex. 32:12 ; No. 14:13-14 ; De. 9:28 ; Jos. 7:9.</span></span><span class="footnote" id="footnote-7"><span class="key">7</span><a class="backref" href="#note-7">20:23</a><span class="text">Lé. 26:13-33.</span></span><span class="footnote" id="footnote-8"><span class="key">8</span><a class="backref" href="#note-8">20:37</a><span class="text">Es. 65:12.</span></span><span class="footnote" id="footnote-9"><span class="key">9</span><a class="backref" href="#note-9">20:40</a><span class="text">Jn. 4:21-24.</span></span>
</div>

<div class="footer"><div class="nav">
<a class="prev" href="EK19.html">&lt;</a>
<a class="home" href="index.html">=</a>
<a class="next" href="EK21.html">&gt;</a>
</div></div>
</body>
</html>